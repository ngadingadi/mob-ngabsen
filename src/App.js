import React, { useEffect } from 'react';
import { StatusBar, ToastAndroid } from 'react-native'; 
import { Provider } from 'react-redux';
import { store, persistor } from './redux/store'
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import { PersistGate } from 'redux-persist/integration/react'
import HomeNavigation from './component/home/homeNavigation';

const App = () => {
  useEffect(() => {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
      interval: 100,
      fastInterval: 5000
    }).then((data) => {
      ToastAndroid.show('Location ' + data, ToastAndroid.LONG,);
    }).catch((err) => {
      ToastAndroid.show('Location ' + err, ToastAndroid.LONG,);
    })
  })

  return (
    <Provider store={store}>
      <StatusBar backgroundColor="#baaf0f" />
      <PersistGate loading={null} persistor={persistor}>
        <HomeNavigation /> 
      </PersistGate>
    </Provider>
  );
};
export default App;
