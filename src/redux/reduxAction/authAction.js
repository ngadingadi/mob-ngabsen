import types from '../action/globalAction'

export const setUserInfo = (payload) => {
    return { type: types.isLoginSuccess, payload: payload }
}