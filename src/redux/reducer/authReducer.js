import types from '../action/globalAction'

const globalState = ({
    isLoading: false,
    isAuthSuccess: false,
    isLoginSuccess: false,
    isLoginFailed: false,
    isRegisterSuccess: false,
    isRegisterFailed: false,
    isDataResponse: '',
    isDataModal: '',
    infoUser: '', 
    respInfoToken: '',
})

const AuthReducer = (state = globalState, action) => {
    switch (action.type) {
        // case actionType.isLoading:
        //     return {
        //         ...state,
        //         isLoading: action.value,
        //     }
        case types.isOauthToken:
            return {
                ...state,
                respInfoToken: action.payload,
                isAuthSuccess: true,
            };
        case types.isLoginSuccess:
            return {
                ...state,
                infoUser: action.payload,
                isAuthSuccess: true,
            };
        case types.isLogout:
            return {globalState};
        default:
            return state;
    }
};
export default AuthReducer;