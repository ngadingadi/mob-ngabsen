import { combineReducers } from 'redux'
import authReducer from './authReducer'
import { persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage';


const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['user']
}

const rootReducer = combineReducers({
    user: authReducer
});

export default persistReducer(persistConfig, rootReducer);

