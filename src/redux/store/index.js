import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { persistStore } from 'redux-persist'
import rootReducer from '../reducer/rootReducer'
import logger from 'redux-logger'

const initialState = {};
const middleware = [thunk,logger]
export const store = createStore(
    rootReducer,
    initialState, 
    compose(
        applyMiddleware(...middleware),
    ),
);
export const persistor = persistStore(store)
export default { store, persistor };