const GlobalAction = {
    isLoginSuccess : "LOGIN_SUCCESS",
    isLoginFailed : "LOGIN_FAILED",
    isRegisterSuccess : "REGISTER_SUCCESS",
    isRegisterFailed : "REGISTER_FAILED",
    isDataResponse : "DATA_RESPONSE",
    isDataModal : "DATA_MODAL",
    isInfoUser : "DATA_USER",
    isLoading : "IS_LOADING",
    isOauthToken : "IS_TOKEN",
    isLogout : "IS_LOGOUT",
}
export default GlobalAction;