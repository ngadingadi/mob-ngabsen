import axios from 'axios';
import { Buffer } from 'buffer';
import { ROOT_HOST } from '../api/indexApi';
import actionType from '../redux/action/globalAction'


export const loginUserAPI = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        dispatch({ type: actionType.isLoading, value: true })
        let tipeClient = 'mobileClient'
        let passClient = '53MU4_!ND4H_P4D4W4KTUNY4'
        let token = Buffer.from(`${tipeClient}:${passClient}`, 'utf8').toString('base64')
        var qs = require('qs');
        var dataReq = qs.stringify({
            'grant_type': 'password',
            'username': data.username,
            'password': data.password
        });
        axios({
            method: "POST",
            url: ROOT_HOST.URL + '/oauth/token',
            auth: {
                username: tipeClient,
                password: passClient
            },
            data: dataReq,
            headers: {
                'Authorization': `Basic ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }).then(resp => {
            console.log(">> "+JSON.stringify(resp))
            return (
                axios({
                    method: "GET",
                    url: ROOT_HOST.URL + '/api/userInfo?email=' + data.username,
                    data: dataReq,
                    headers: {
                        'Authorization': `Bearer ${resp.data.access_token}`,
                        'Content-Type': 'application/applicaation/json',
                    },
                }).then(resp2 => {
                    if (resp2.data.rc === '00') {
                        resolve(true)
                        dispatch({ type: actionType.isLoginSuccess, payload: resp2.data })
                        dispatch({ type: actionType.isOauthToken, payload: resp.data })
                        dispatch({ type: actionType.isLoading, value: false })
                    } else {
                        alert(resp2.data.detail)
                    }
                }).catch((error2) => {
                    alert(error2.data.detail)
                })
            )
        }).catch((error) => {
            // reject(true)
            alert("Bad Credential ...!!")
        })

    })
}

export const logOut = () => (dispatch) => {
    dispatch({ type: actionType.isLogout })
}