"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.stylesHome = void 0;

var _reactNative = require("react-native");

var stylesHome = _reactNative.StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F2E652"
  },
  welcomeHomeCompanyName: {
    flex: 2,
    marginTop: 10,
    maxHeight: 20,
    fontSize: 12,
    fontFamily: 'sans-serif-light',
    fontStyle: 'italic',
    marginLeft: 10,
    marginRight: 10
  },
  panelA: {
    flex: 3,
    maxHeight: 30,
    marginTop: 3,
    marginLeft: 10,
    marginRight: 10,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  panelA1: {
    fontSize: 17,
    fontFamily: 'sans-serif-light',
    marginTop: 30
  },
  panelA2: {
    fontSize: 13,
    fontFamily: 'sans-serif-light',
    fontStyle: 'italic',
    fontWeight: 'bold'
  },
  panelA3: {
    marginLeft: 8,
    height: 60,
    width: 60,
    borderRadius: 40,
    borderColor: 'white',
    backgroundColor: "#F2E652",
    borderWidth: 2
  },
  panelB: {
    flex: 4,
    flexDirection: 'row',
    paddingTop: 15
  },
  panelC: {
    flex: 1,
    backgroundColor: "blue",
    marginTop: 50
  },
  panelD: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignContent: 'space-around',
    justifyContent: 'flex-end',
    marginHorizontal: 10,
    marginVertical: 10
  },
  panelD1: {
    fontSize: 11
  },
  nav1: {
    marginHorizontal: 17,
    flexDirection: 'row',
    paddingTop: 15
  },
  nav2: {
    marginHorizontal: 17,
    marginTop: 8
  },
  nav3: {
    flexDirection: 'row',
    paddingTop: 20,
    paddingBottom: 14
  }
});

exports.stylesHome = stylesHome;