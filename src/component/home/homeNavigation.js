import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { Attendance } from '../main/attendance/attendance';
import { Leave } from '../main/leave/leave';
import { Overtime } from '../main/overtime/overtime';
import { ComingSoon } from '../main/comingSoon/comingSoon';
import { Clockinout } from '../main/clockinout/clockinout';
import { Login } from '../login/login'
import { homeDashboard } from '../main/homeDashboard/homeDashboard';
import { useSelector } from 'react-redux';
import { Logout } from '../logout/logout';

const Stack = createStackNavigator();
export default function HomeNavigation() {

    const results = useSelector(state => state); 
    var isAuth = results.user.isAuthSuccess; 
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                {isAuth ? (
                    // No token found, user isn't signed in
                    <>
                        <Stack.Screen name="home" component={homeDashboard} />
                        <Stack.Screen name="attendance" component={Attendance} screenOptions={{ headerShown: true }}/>
                        <Stack.Screen name="leave" component={Leave} />
                        <Stack.Screen name="overtime" component={Overtime} />
                        <Stack.Screen name="reimbusment" component={ComingSoon} />
                        <Stack.Screen name="clockinout" component={Clockinout} />
                        <Stack.Screen name="logout" component={Logout} screenOptions={{ headerShown: true }}/>
                    </>
                ) : (
                    // User is signed in
                    <Stack.Screen name="login" component={Login} />
                )}
            </Stack.Navigator>
        </NavigationContainer>
    );
}