import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { useDispatch } from 'react-redux';
import { logOut } from '../../api/loginUserApi'


const styles = StyleSheet.create({
    container: {
        marginVertical:100,
        marginHorizontal:10,
    },
    buttonStyle: {
        color:"#fff",
        backgroundColor: "#baaf0f",
        borderRadius:40,

    },
    title:{
        color:"#ffff"
    }
});

export const Logout = ({ navigation }) => {
    const dispatch = useDispatch();
    const logOutFunc = () => {
        dispatch(logOut())
        navigation.navigate("home")
    }
    return (
        <View style={styles.container}>
            <Button buttonStyle={styles.buttonStyle} titleStyle={styles.title} title="SIGN OUT"
                onPress={() => logOutFunc()}
            />
        </View>
    )
}