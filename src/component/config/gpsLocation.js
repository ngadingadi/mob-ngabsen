import React, { useState, useEffect } from 'react';
import { View, Platform, PermissionsAndroid, ToastAndroid, Alert, Text } from 'react-native';
import Geolocation from 'react-native-geolocation-service';

const GpsLocation = () => {
    const [loc, setLoc] = useState();

    const hasLocationPermission = async () => {
        if (Platform.OS === 'android' && Platform.Version < 23) {
            return true;
        }

        const hasPermission = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (hasPermission) {
            return true;
        }

        const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (status === PermissionsAndroid.RESULTS.GRANTED) {
            return true;
        }

        if (status === PermissionsAndroid.RESULTS.DENIED) {
            ToastAndroid.show(
                'Location permission denied by user.',
                ToastAndroid.LONG,
            );
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG,);
        }

        return false;
    };

    const getLocation = async () => {
        const permisiion = await hasLocationPermission();

        if (!permisiion) {
            return;
        }

        Geolocation.getCurrentPosition(
            (position) => {
                setLoc(position)
                console.log(position);
            },
            (error) => {
                Alert.alert(`Code ${error.code}`, error.message);
                console.log(error);
            },
            {
                accuracy: {
                    android: 'high',
                    ios: 'best',
                },
                enableHighAccuracy: true,
                timeout: 15000,
                maximumAge: 10000,
                distanceFilter: 0,
            }
        )
    }

    return(
        getLocation()
    )

}
export default GpsLocation;