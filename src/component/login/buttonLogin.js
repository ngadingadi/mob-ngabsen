import React, { useState } from 'react';
import { Text } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import { stylesLogin } from './loginStyle'

export const ButtonLogin = ({ props }) => {
    const foo = useState(props)
    console.log(">>>> " + foo.loading)
    return (
        <Icon name="login" size={40} buttonStyle={stylesLogin.buttonSubmit}  color="#baaf0f" />
    )
};