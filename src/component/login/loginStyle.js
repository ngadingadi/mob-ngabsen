import { StyleSheet } from "react-native";

export const stylesLogin = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: '30%',
        marginLeft: '5%',
        marginRight: '5%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageLogo: {
        height: 180,
        width: 180
    },
    viewSubmit: {
        flex: 2,
        flexDirection: 'row',
        alignSelf:'center',
    },
    buttonSubmit: {
        backgroundColor: '#93D300',
        borderRadius: 10, 
        height:50, 
    },
    viewSubmit2: {
        flex: 3,
        flexDirection: 'row',
        alignSelf:'flex-end',
    }
})
