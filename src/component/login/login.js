import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { View } from 'react-native'
import { stylesLogin } from './loginStyle'
import Icon from 'react-native-vector-icons/AntDesign';
import { Image, Input } from 'react-native-elements';
import logo from '../../assets/logo_ngadi2.png'
import { loginUserAPI } from '../../api/loginUserApi'
import { SafeAreaView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch();
    const results = useSelector(state => state);
    const onLoginUser = () => {
        let body = {
            username: username,
            password: password
        }
        dispatch(loginUserAPI(body))
    }
    return (
        <SafeAreaView>
            <KeyboardAwareScrollView>
                <View style={stylesLogin.container}>
                    <Image source={logo} style={stylesLogin.imageLogo} />
                    <Input onChangeText={(username) => setUsername(username)} value={username} name="username" containerStyle={stylesLogin.texInput} placeholder="email" />
                    <Input onChangeText={(password) => setPassword(password)} value={password} name="password" containerStyle={stylesLogin.texInput} secureTextEntry={true} placeholder="******" />
                    <View style={stylesLogin.viewSubmit}>
                        <Icon name="login" size={40} buttonStyle={stylesLogin.buttonSubmit} onPress={onLoginUser} color="#baaf0f" />
                    </View>
                </View>
            </KeyboardAwareScrollView>
        </SafeAreaView>
    )
};
