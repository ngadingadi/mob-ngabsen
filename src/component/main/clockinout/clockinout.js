import React, { useEffect } from 'react';
import { Text, View, PermissionsAndroid, StyleSheet, Platform, ToastAndroid } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import { useState } from 'react';
import { Alert } from 'react-native';
import { useRoute } from '@react-navigation/core';

export function Clockinout() {
    const [forceLocation, setForceLocation] = useState(true)
    const [highAccuracy, setHighAccuracy] = useState(true)
    const [loading, setLoading] = useState(false)
    const [showLocationDialog, setShowLocationDialog] = useState(false)
    const [location, setLocation] = useState({})
    const [permission, setPermission] = useState(false)
    const [mocked, setMocked] = useState(false)
    const [latitude, setLatitude] = useState(0)
    const [longitude, setLongitude] = useState(0)
    const route = useRoute();

    const styles = StyleSheet.create({
        container: {
            ...StyleSheet.absoluteFillObject,
            marginVertical: 20,
            height: 250,
            position: "absolute",
            justifyContent: 'flex-end',
            alignItems: 'center',
        },
        map: {
            ...StyleSheet.absoluteFillObject,
        },
    });

    const hasLocationPermission = async () => { 
        const hasPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,);
        
        if (hasPermission) {
            return true;
        }

        const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (status === PermissionsAndroid.RESULTS.GRANTED) { 
            return true;
        }

        if (status === PermissionsAndroid.RESULTS.DENIED) { 
            ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG,);
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) { 
            ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG,);x
        } 
        console.log("status "+status)
        return false;
    };

    const getLocation = async () => {
        const hasLocationPermissiona = await hasLocationPermission();
        if (!hasLocationPermissiona) {
            return;
        }
        Geolocation.getCurrentPosition(
            (position) => {
                // setLocation(position.coords)
                setLatitude(position.coords.latitude)
                setLongitude(position.coords.longitude)
                setLoading(false)
                setMocked(position.mocked)
                // console.log("position  : " + JSON.stringify(position));
            },
            (error) => {
                setLoading(false);
                Alert.alert(`Code ${error.code}`, error.message);
                console.log(error);
            },
            {
                accuracy: {
                    android: 'high',
                    ios: 'best',
                },
                enableHighAccuracy: highAccuracy,
                timeout: 15000,
                maximumAge: 10000,
                distanceFilter: 0,
                forceRequestLocation: forceLocation,
                showLocationDialog: showLocationDialog,
            },
        );
    };

    useEffect(() => {
        hasLocationPermission()
        getLocation()
    })



    if (hasLocationPermission()) {
        return (
            <View>
                <Text>Hai aku clockinout {route.params.key}</Text> 
                <View style={styles.container}>
                    <MapView
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        style={styles.map}
                        scrollEnabled={true}
                        region={{
                            latitude: latitude,
                            longitude: longitude,
                            latitudeDelta: 0.005,
                            longitudeDelta: 0.0121,
                        }}
                    >
                        <Marker 
                            coordinate={{latitude: latitude, longitude: longitude}}
                            title="kamu sedang disini"
                            description=""
                        />
                    </MapView>
                </View>
            </View>

        )
    } else {
        return (
            <View>
                <Text>Please enable GPS Location</Text>
            </View>
        )
    }


};