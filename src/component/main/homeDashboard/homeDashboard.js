import React, { useEffect, useState } from 'react';
import { Text, View } from 'react-native';
import { stylesHome } from './homeStyle'
import moment from 'moment-timezone'
import { Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Mat from 'react-native-vector-icons/MaterialCommunityIcons';
import Ant from 'react-native-vector-icons/AntDesign';
import { Button } from 'react-native-elements';
import { FlatGrid } from 'react-native-super-grid';
import { useSelector } from 'react-redux';


export const homeDashboard = ({ navigation }) => {
    const results = useSelector(state => state);
    const [time, setTime] = useState('')
    const [items, setItems] = useState([
        { id: 1, nameIcon: "coins", color: "green", label: "Attendance", navigation: "attendance" },
        { id: 2, nameIcon: "walking", color: "red", label: "Leave", navigation: "leave" },
        { id: 3, nameIcon: "tachometer-alt", color: "orange", label: "Overtime", navigation: "overtime" },
        { id: 4, nameIcon: "diagnoses", color: "magenta", label: "Reimbusment", navigation: "reimbusment" },
    ])

    useEffect(() => {
        const interval = setInterval(() => { setTime(moment().tz('Asia/Bangkok').format("DD-MMM-YYYY HH:mm:ss")) }, 1000)
        return () => clearInterval(interval)
    }, []);

    return (
        <View style={stylesHome.container}>
            <View style={stylesHome.welcomeHomeCompanyName}>
                <Text>{results.user.infoUser.data.company_name}</Text>
            </View>
            <View style={stylesHome.panelA}>
                <View>
                    <Text style={stylesHome.panelA1}>
                        <Text>{results.user.infoUser.data.employee_name}</Text>
                    </Text>
                    <Text style={stylesHome.panelA2}>
                        <Text>{results.user.infoUser.data.level_name}</Text>
                    </Text>
                </View>
                <View>
                    <Image source={{ uri: results.user.infoUser.data.employee_picture }} style={stylesHome.panelA3} />
                </View>
            </View>
            <View style={stylesHome.panelC}>
                <View style={stylesHome.panelD}>
                    <View style={stylesHome.panelD1}>
                        <FlatGrid
                            horizontal={false}
                            scrollEnabled={false}
                            itemDimension={70}
                            spacing={10}
                            data={items}
                            renderItem={({ item }) => (
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <Button icon={<Icon name={item.nameIcon} size={30} color={item.color} />} buttonStyle={{ backgroundColor: "none" }} onPress={() => navigation.navigate(item.navigation)} />
                                    <Text style={stylesHome.panelD1}>{item.label}</Text>
                                </View>
                            )}
                        />
                    </View>
                </View>


                <View style={stylesHome.panelD}>
                    <View>
                        <Text style={{ fontSize: 11, alignSelf: 'center' }}>{time}</Text>
                    </View>
                    <View style={stylesHome.panelD2}>
                        <View style={{ width: "50%" }}>
                            <Text style={stylesHome.panelD4}>
                                {/* <Button icon={<Mat name="clock-in" size={40} color="#6EA1FF" />} buttonStyle={{ backgroundColor: "none" }} onPress={() => navigationnavigation.navigate({ name: "clockinout", key: "clockin" })} /> */}
                                <Button icon={<Mat name="clock-in" size={40} color="#6EA1FF" />} buttonStyle={{ backgroundColor: "none" }} onPress={() => navigation.navigate('clockinout', { key: 'clockin' })} />
                            </Text>
                            <Text style={stylesHome.panelD4}>Clock In</Text>
                        </View>
                        <View style={{ width: "50%" }}>
                            <Text style={stylesHome.panelD4}>
                                <Button icon={<Mat name="clock-out" size={40} color="red" />} buttonStyle={{ backgroundColor: "none" }} onPress={() => navigation.navigate('clockinout', { key: 'clockout' })} />
                            </Text>
                            <Text style={stylesHome.panelD4}>Clock Out</Text>
                        </View>
                    </View>
                </View>
            </View>

            <View style={{ height: 40, flexDirection: 'row', backgroundColor: 'white' }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Ant name="message1" size={20} />
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Icon name="user-tie" size={20} onPress={() => handleClick} />
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Button icon={<Mat name="exit-to-app" size={30} />} buttonStyle={{ backgroundColor: "none" }} onPress={() => navigation.navigate('logout')}/>
                </View>
            </View>
        </View>
    )

}