import { Dimensions, StyleSheet } from "react-native";
const DeviceWidth = Dimensions.get('window').width;
export const stylesHome = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F2E652",
    },
    welcomeHomeCompanyName: {
        flex: 2,
        marginTop: 10,
        maxHeight: 20,
        fontSize: 12,
        fontFamily: 'sans-serif-light',
        fontStyle: 'italic',
        marginLeft: 10,
        marginRight: 10
    },
    panelA: {
        flex: 3,
        maxHeight: 30,
        marginTop: 3,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: "row",
        justifyContent: "space-between",
    },
    panelA1: {
        fontSize: 17,
        fontFamily: 'sans-serif-light',
        marginTop: 30
    },
    panelA2: {
        fontSize: 13,
        fontFamily: 'sans-serif-light',
        fontStyle: 'italic',
        fontWeight: 'bold',
    },
    panelA3: {
        marginLeft: 8,
        height: 60,
        width: 60,
        borderRadius: 40,
        borderColor: 'white',
        backgroundColor: "#F2E652",
        borderWidth: 2
    },
    panelB: {
        flex: 4,
        flexDirection: 'row',
        paddingTop: 15
    },
    panelC: {
        flex: 1,
        marginTop: 50,
    },
    panelD: {
        marginHorizontal: 10,
        marginVertical: 10,
        borderWidth: 0.5,
        borderRadius: 10,
        backgroundColor: 'white',
    },
    panelD1: {
        fontSize: 10,
    },
    panelD2: {
        fontSize: 10,
        height: 80,
        flexDirection: "row",
        justifyContent: "space-around"
    },
    panelD4: {
        fontSize: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    panelD5: {
        fontSize: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    panelD3: {
        width: "50%"
    },
    nav1: {
        marginHorizontal: 17,
        flexDirection: 'row',
        paddingTop: 15
    },
    nav2: {
        marginHorizontal: 17,
        marginTop: 8
    },
    nav3: {
        flexDirection: 'row',
        paddingTop: 20,
        paddingBottom: 14
    }
})